#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>

#define NUM_OPTIONS 2

#define TSEPT_VERSION 6

#ifdef DEBUG
	#define DEBUGMODE 1
#else
	#define DEBUGMODE 0
#endif

//Debug printf
#define dprintf(...) if (DEBUGMODE) printf("DEBUG: "), printf(__VA_ARGS__);

char *exception_names[] = {
	"Illegal Instruction",
	"Syscall failed",
	"Unable to allocate memory",
	"Out of bounds memory access",
	"Stack overflow",
	"Stack underflow",
	"Inexistant syscall"
};

int reg_a = TSEPT_VERSION;
int reg_b;
int reg_s;
int reg_c;
int reg_d;
int reg_e;
int reg_x;
int prg_pos;

int stack_a[256];
int sp_a = 0;

int stack_b[256];
int sp_b = 0;

int stack_temp[256];

unsigned char *heap;
int heap_size = 65536;

//Pops from stack A.
int stack_pop() {
	sp_a--;
	if (sp_a<0) exception(6);
	if (sp_a>255) exception(5);
	return stack_a[sp_a];
}

//Pushes to stack A.
void stack_push(int val) {
	stack_a[sp_a] = val;
	sp_a++;
	if (sp_a>255) exception(5);
}

void swap(int *a, int *b) {
	if (!a || !b) return;
	dprintf("%d %d\n", *a, *b);
	int temp = *a;
	*a = *b;
	*b = temp;
	dprintf("%d %d\n", *a, *b);
}

void exception(int num) {
	printf("\nException %d (%s) at %d. Program Halted.\n", num, 
		exception_names[(num-1)%8], prg_pos+1);
	printf("First Stack: ");
	for (int i=0; i<sp_a; i++) {
		printf("%d ", stack_a[i]);
	}
	printf("\n");
	printf("Second Stack: ");
	for (int i=0; i<sp_b; i++) {
		printf("%d ", stack_b[i]);
	}
	printf("\n");
	printf("Register A: %d\n", reg_a);
	printf("Register B: %d\n", reg_b);
	printf("Register S: %d\n", reg_s);
	printf("Register C: %d\n", reg_c);
	printf("Register D: %d\n", reg_d);
	printf("Register E: %d\n", reg_e);
	printf("Register X: %d\n", reg_x);
	exit(255);
}

int filesize(FILE *fp) {
	int oo = ftell(fp);
	fseek(fp, 0, SEEK_END);
	int len = ftell(fp);
	fseek(fp, oo, SEEK_SET);
	return len;
}

void do_syscall() {
	int tmp;
	switch (reg_a) {
	case 0: //read
		if (reg_d>heap_size || reg_x > (heap_size-reg_d) || reg_d<0) exception(4);
		read(reg_s, heap+reg_d, reg_x);
		break;
	case 1: //write
		if (reg_d>heap_size || reg_x > (heap_size-reg_d) || reg_d<0) exception(4);
		write(reg_s, heap+reg_d, reg_x);
		break;
	case 2: //open
		if (reg_d>heap_size || reg_d<0) exception(4);
		reg_s = open(heap+reg_d, O_RDWR|O_SYNC);
		break;
	case 3: //close
		close(reg_s);
		break;
	case 4: //create
		if (reg_d>heap_size || reg_d<0) exception(4);
		close(open(heap+reg_d, O_RDWR|O_SYNC|O_CREAT));
		break;
	case 5: //link
		if (reg_d>heap_size || reg_d<0) exception(4);
		if (reg_x>heap_size || reg_x<0) exception(4);
		link(heap+reg_d, heap+reg_x);
		break;
	case 6: //delete
		if (reg_d>heap_size || reg_d<0) exception(4);
		unlink(heap+reg_d);
		break;
	case 7: //fork
		if (tmp = fork()) {
			reg_s = tmp;
		}
		break;
	case 8: //getpid
		reg_s = getpid();
		break;
	case 9: //getppid
		reg_s = getppid();
		break;
	case 10:; //exec
		if (reg_d>heap_size || reg_d<0) exception(4);
		char *args[16];
		if (reg_s > 15) exception(2);
		for (int i=0; i<reg_s; i++) {
			args[i] = heap+stack_pop();
		}
		execv(heap+reg_d, args);
		break;
	case 11: //chmod
		if (reg_d>heap_size || reg_d<0) exception(4);
		chmod(heap+reg_d, reg_s);
		break;
	case 12: //kill
		kill(reg_s, reg_d);
		break;
	case 13: //rename
		if (reg_d>heap_size || reg_d<0) exception(4);
		if (reg_x>heap_size || reg_x<0) exception(4);
		rename(heap+reg_d, heap+reg_x);
		break;
	case 14:  //mkdir
		mkdir(heap+reg_d, 0664);
		break;
	case 15:  //rmdir
		rmdir(heap+reg_d);
		break;
	case 16:  //time
		reg_s = time(0);
		break;
	default:
		exception(7);
	}
}

void interpret(FILE *fp) {
	dprintf("Interpreter\n");
	
	int flen;
	char *buffer = malloc(flen = filesize(fp));
	
	if (!buffer) exception(3);
	
	int read_bytes = fread(buffer, 1, flen, fp);
	
	dprintf("Read %u bytes\n", read_bytes);
	
	int temp;
	int pos_cmodified = 0;
	
	for (prg_pos=0; prg_pos<flen; prg_pos++) {
		switch (buffer[prg_pos]) {
			case 'A':
				reg_a += stack_pop();
				break;
			case 'S':
				reg_a -= stack_pop();
				break;
			case 'I':
				reg_a++;
				break;
			case 'D':
				reg_a--;
				break;
			case 'X':
				reg_a ^= stack_pop();
				break;
			case 'a':
				reg_a &= stack_pop();
				break;
			
			case 'B':
				swap(&reg_a, &reg_b);
				break;
			case 'w':
				swap(&reg_d, &reg_e);
				break;
			case 'x':
				reg_a = 0;
				break;
			case 'K':
				swap(&reg_s, &reg_b);
				break;
			case 'b':
				swap(&reg_x, &reg_e);
				break;
			case 'k':
				reg_a = reg_c;
				break;
			
			case 'P':
				stack_push(reg_a);
				break;
			case 'p':
				reg_a = stack_pop();
				break;
			case 'W':
				memcpy(stack_temp, stack_a, 256);
				memcpy(stack_a, stack_b, 256);
				memcpy(stack_b, stack_temp, 256);
				break;
			case 'C':
				reg_c = stack_pop();
				pos_cmodified = prg_pos;
				break;
			case 'd':
				reg_d = stack_pop();
				break;
			case 'H':
				if (reg_d > heap_size) exception(4);
				stack_push(heap[reg_d]);
				break;
			case 'h':
				((int*)heap)[reg_d] = stack_pop();
				break;
			case 'R':
				stack_push(reg_d);
				break;
			case 'c':
				stack_push(reg_s);
				break;
			case 'l':
				reg_s = stack_pop();
				break;
			
			case 'J':
				prg_pos += stack_pop();
				break;
			case 'i':
				if (reg_a) prg_pos += stack_pop();
				break;
			case 'L':
				if (reg_c) {
					reg_c--;
					prg_pos = pos_cmodified+1;
				}
				break;
			
			case '!':
				putchar(reg_a);
				break;
			case '?':
				reg_a = getchar();
				break;
			
			case '\n':
			case ' ': break;
			
			case '/':
				prg_pos++;
				while (buffer[prg_pos] != '/') prg_pos++;
				break;
			
			case 's':
				do_syscall();
			
			default:
				printf("%x", buffer[prg_pos]);
				exception(1);
		}
	}
	if (DEBUGMODE) exception(0);
}

void compile(FILE *fp) {
	dprintf("Compiler\n");
}

struct {
	char *name;
	void (*function)(FILE*);
} options[NUM_OPTIONS] = {
	{"-i", interpret},
	{"-c", compile}
};

int main(int argc, char **argv) {
	if (argc<3) {
		fprintf(stderr, "Usage: tsept [-i/-c] [file]\nTsept, an esolang with 28 instructions and syscalls.\n");
		return 1;
	}
	
	FILE *fp = fopen(argv[2], "r");
	
	heap = malloc(heap_size = 65536);
	if (!heap) exception(3);
	
	if (!fp) {
		fprintf(stderr, "Unable to open file %s, because %s.\n", argv[2], strerror(errno));
	}
	
	for (int i=0; i<NUM_OPTIONS; i++) {
		if (!strcmp(options[i].name, argv[1])) {
			options[i].function(fp);
			return 0;
		}
	}
	
	fprintf(stderr, "Can't inderstand invalid option %s.\n", argv[1]);
	return 1;
}
