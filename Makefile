all: clean build/main

clean:
	rm -f build/*

build/main:
	gcc src/*.c -o build/main $(CFLAGS)

